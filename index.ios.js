import React from 'react';
import { AppRegistry } from 'react-native';
import app from './src/index.js';

AppRegistry.registerComponent('okcare', () => app);
