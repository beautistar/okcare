import LocalizedStrings from 'react-native-localization';
import en from './local/en';
import enUS from './local/en-us';
import zh from './local/zh';

let localized = new LocalizedStrings({"en-US": enUS, en: en, zh: zh});

export default localized;
