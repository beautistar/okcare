import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Input,
  Content,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import ScrollableTabView, {DefaultTabBar} from 'react-native-scrollable-tab-view';
import localized from '../Localized';
import PurchaseService from './PurchaseService/PurchaseService';
import MyService from './MyService/MyService';
import ServiceRecord from './ServiceRecord/ServiceRecord';
const theme = require('../theme/index.js');

class Home extends Component {
  _viewProfile() {
    Actions.PersonalFiles();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left/>
          <Body>
            <Title style={_styles.whiteColor}>OK CARE</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this._viewProfile()}>
              <Icon active name='contact' style={_styles.whiteColor}/>
            </Button>
          </Right>
        </Header>
        <ScrollableTabView renderTabBar={() => <DefaultTabBar backgroundColor={themePrimaryColor}/>} ref={(tabView) => {
          this.tabView = tabView;
        }} initialPage={1} tabBarActiveTextColor='white' tabBarInactiveTextColor='white' tabBarUnderlineStyle={_styles.buttonSecondaryColor}>
          <ScrollView tabLabel={localized.home.purchaseService}>
            <PurchaseService/>
          </ScrollView>
          <ScrollView tabLabel={localized.home.myService}>
            <MyService/>
          </ScrollView>
          <ScrollView tabLabel={localized.home.serviceRecord}>
            <ServiceRecord/>
          </ScrollView>
        </ScrollableTabView>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  buttonSecondaryColor: {
    backgroundColor: themeSecondaryColor
  },
  whiteColor: {
    color: 'white'
  }
};

export default Home;
