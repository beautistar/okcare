import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Thumbnail,
  CheckBox,
  Input
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";
const theme = require('../../theme/index.js');
import Hr from '../../lib/hr';

class AppointmentProfessionalCare1 extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.appointmentprofessionalcare1.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Header style={_styles.secondHeader} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          {/* <Left/> */}
          <Body style={_styles.heading2}>
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>1</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare1.browse}</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare1.serviceticket}</Text>
           </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundView}>
              <Text style={_styles.roundText}>2</Text>
              <Text style={_styles.roundText}>{localized.appointmentprofessionalcare1.reservation}</Text>
              <Text style={_styles.roundText}>{localized.appointmentprofessionalcare1.date}</Text>
            </View>
          </Body>
          {/* <Right/> */}
        </Header>
        <Content padder>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.appointmentprofessionalcare1.serviceuser}</Text>
            </View>
            <View style={_styles.padTop}>
              <Grid>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.padRight}>{localized.appointmentprofessionalcare1.abbservation}</Text>
                    <Text style={_styles.padLeft}>{localized.appointmentprofessionalcare1.name2}</Text>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Text>ID：A0122122(3)</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col size={2} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.dob}
                      :
                    </Text>
                    <Text>{localized.appointmentprofessionalcare1.dob1}</Text>
                  </Col>
                  <Col size={1} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.age}
                      :
                    </Text>
                    <Text>{localized.appointmentprofessionalcare1.age1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col size={4} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.h}
                      :
                    </Text>
                    <Text>{localized.appointmentprofessionalcare1.h1}</Text>
                  </Col>
                  <Col size={3} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.w}
                      :
                    </Text>
                    <Text>{localized.appointmentprofessionalcare1.w1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Text>{localized.appointmentprofessionalcare1.relation}</Text>
                </Row>
                <Row style={_styles.padTop}>
                  <Text>{localized.appointmentprofessionalcare1.medical}</Text>
                </Row>
                <Row style={_styles.padTop}>
                  <Col>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.address}
                      :
                    </Text>
                  </Col>
                  <Col>
                    <Text>{localized.appointmentprofessionalcare1.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.number}:</Text>
                    <Text>6767 6767</Text>
                  </Col>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.hnumber}:</Text>
                    <Text>2267 6767</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare1.healthservice}:
                    </Text>
                    <Text>{localized.appointmentprofessionalcare1.no}</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.appointmentprofessionalcare1.emergency}</Text>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row>
                <Col size={4}>
                  <Text>{localized.appointmentprofessionalcare1.amway}</Text>
                </Col>
                <Col size={2}>
                  <Text>6767 6767</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.appointmentprofessionalcare1.servicearea}</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.appointmentprofessionalcare1.service}A,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}A,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}A,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}A,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}A</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.appointmentprofessionalcare1.service}B,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}B,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}B,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}B,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}B</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.appointmentprofessionalcare1.service}C,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}C,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}C,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}C,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}C</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.appointmentprofessionalcare1.service}D,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}D,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}D,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}D,</Text>
              <Text>{localized.appointmentprofessionalcare1.service}D</Text>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.padTopBottom}>
            <Button block style={_styles.buttonPrimaryColor} onPress={() => Actions.AppointmentProfessionalCare2()}>
              <Text>{localized.appointmentprofessionalcare1.buttonReserve}</Text>
            </Button>
          </View>
          <View style={_styles.moveCenterPadding}>
            <Text style={_styles.primaryColor}>{localized.appointmentprofessionalcare1.terms}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  secondHeader: {
    backgroundColor: themePrimaryColor,
    marginTop: -5,
    height: 90
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  textBold: {
    fontWeight: 'bold'
  },
  heading: {
    flex: 5,
    flexDirection: 'row'
  },
  heading2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 100,
    paddingRight: 100,
  },
  roundViewActive: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundTextActive: {
    color: themePrimaryColor,
    fontSize: 10,
    fontWeight: 'bold'
  },
  roundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundText: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  padRight: {
    paddingRight: 5
  },
  padLeft: {
    paddingLeft: 5
  },
  padTop: {
    paddingTop: 10
  },
  padBottom: {
    paddingBottom: 10
  },
  padTopBottom: {
    paddingTop: 10,
    paddingBottom: 10
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  service: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5
  },
  rowView: {
    flexDirection: 'row'
  },

  moveCenterPadding: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 20
  }
};

export default AppointmentProfessionalCare1;
