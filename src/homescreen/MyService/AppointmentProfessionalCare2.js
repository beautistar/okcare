import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Thumbnail,
  CheckBox,
  Input
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Calendar} from 'react-native-calendars';
const theme = require('../../theme/index.js');
import Hr from '../../lib/hr';
import Modal from '../../lib/Modal';
import localized from '../../Localized';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

let today = new Date();

class AppointmentProfessionalCare2 extends Component {
  constructor(props) {
    super(props);
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

  _goBack() {
    Actions.pop();
  }

  _onDateClick(day) {
    this.refs.ocmodal1.open();
    console.log(day);
  }

  _closeOneOpenTwo() {
    this.refs.ocmodal1.close();
    this.refs.ocmodal2.open();
  }

  _renderModal2() {
    return (
      <Modal ref={'ocmodal2'} backdropPressToClose={false} swipeToClose={false} onClosed={this.onClose} onOpened={this.onOpen} onClosingState={this.onClosingState} position={"bottom"} style={_styles.modalHeight}>
        <Container>
          <Header>
            <Left/>
            <Body style={_styles.heading}>
              <Title>{localized.appointmentprofessionalcare2.dateofbooking}</Title>
            </Body>
            <Right>
              <Button transparent onPress={() => this.refs.ocmodal2.close()}>
                <Icon name='md-close-circle' style={_styles.secondaryColor}/>
              </Button>
            </Right>
          </Header>
          <Content padder>
            <View style={_styles.padTopBottom}>
              <Grid>
                <Row style={_styles.smPadBottom}>
                  <Col size={2}>
                    <Text>1. {localized.appointmentprofessionalcare2.date1}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{localized.appointmentprofessionalcare2.time1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadBottom}>
                  <Col size={2}>
                    <Text>2. {localized.appointmentprofessionalcare2.date2}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{localized.appointmentprofessionalcare2.time1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadBottom}>
                  <Col size={2}>
                    <Text>3. {localized.appointmentprofessionalcare2.date3}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{localized.appointmentprofessionalcare2.time1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadBottom}>
                  <Col size={2}>
                    <Text>4. {localized.appointmentprofessionalcare2.date4}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{localized.appointmentprofessionalcare2.time1}</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={_styles.padTopBottom}>
              <Hr style={_styles.primaryColor}/>
            </View>
            <View>
              <Button block style={_styles.modalButton}>
                <Text>{localized.appointmentprofessionalcare2.confirmmakeappointment}</Text>
              </Button>
            </View>
          </Content>
        </Container>
      </Modal>
    );
  }

  _renderModal1() {
    return (
      <Modal ref={'ocmodal1'} backdropPressToClose={false} swipeToClose={false} onClosed={this.onClose} onOpened={this.onOpen} onClosingState={this.onClosingState} position={"bottom"} style={_styles.modalHeight}>
        <Container>
          <Header>
            <Left/>
            <Body style={_styles.heading3}>
              <Title>{localized.appointmentprofessionalcare2.date1}</Title>
            </Body>
            <Right>
              <Button transparent onPress={() => this.refs.ocmodal1.close()}>
                <Icon name='md-close-circle' style={_styles.secondaryColor}/>
              </Button>
            </Right>
          </Header>
          <Content padder>
            <View style={_styles.padTopBottom}>
              <Grid>
                <Row>
                  <Col>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare2.workinghrs}
                      :
                    </Text>
                  </Col>
                  <Col>
                    <Text>09:00 {localized.appointmentprofessionalcare2.to}
                      18:00</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={_styles.padTopBottom}>
              <Input multiline={true} placeholder={localized.appointmentprofessionalcare2.remark}/>
            </View>
            <View style={_styles.padTopBottom}>
              <Hr style={_styles.primaryColor}/>
            </View>
            <View>
              <Button block style={_styles.modalButton} onPress={() => this._closeOneOpenTwo()}>
                <Text>{localized.appointmentprofessionalcare2.confirm}</Text>
              </Button>
            </View>
          </Content>
        </Container>
      </Modal>
    );
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.appointmentprofessionalcare2.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Header style={_styles.secondHeader} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          {/* <Left/> */}
          <Body style={_styles.heading2}>
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>1</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare2.browse}</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare2.serviceticket}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
          <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>2</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare2.reservation}</Text>
              <Text style={_styles.roundTextActive}>{localized.appointmentprofessionalcare2.date}</Text>
            </View>
          </Body>
          {/* <Right/> */}
        </Header>
        <Content padder>
          <View style={_styles.padTopBottom}>
            <View>
              <Grid>
                <Row style={_styles.padTopBottom}>
                  <Col style={_styles.moveCenter}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare2.block}
                      :
                    </Text>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Thumbnail style={_styles.roundViewBlock}>
                      <Text style={_styles.roundText}>4 {localized.appointmentprofessionalcare2.times}</Text>
                    </Thumbnail>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Text style={_styles.textBold}>{localized.appointmentprofessionalcare2.serviceticket1}
                      :
                    </Text>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Thumbnail style={_styles.roundView}>
                      <Text style={_styles.roundText}>2 {localized.appointmentprofessionalcare2.zhang}</Text>
                    </Thumbnail>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Calendar style={_styles.calendar} current={today} minDate={'2012-05-10'} markingType={'interactive'} hideArrows={false} onDayPress={(day) => this._onDateClick(day)}/>
          </View>
          <View style={_styles.lgPadTopBottom}>
            <Button block style={_styles.buttonPrimaryColor}>
              <Text>{localized.appointmentprofessionalcare2.next}</Text>
            </Button>
          </View>
        </Content>
        {this._renderModal1()}
        {this._renderModal2()}
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  secondHeader: {
    backgroundColor: themePrimaryColor,
    marginTop: -5,
    height: 90
  },
  primaryColor: {
    color: themePrimaryColor
  },
  secondaryColor: {
    color: themeSecondaryColor
  },
  whiteColor: {
    color: 'white'
  },
  textBold: {
    fontWeight: 'bold'
  },
  heading: {
    flex: 5,
    flexDirection: 'row'
  },
  heading3: {
    flex: 2,
    flexDirection: 'row'
  },
  heading2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 100,
    paddingRight: 100,
  },
  rowView: {
    flexDirection: 'row'
  },
  padTopBottom: {
    paddingTop: 5,
    paddingBottom: 5
  },
  lgPadTopBottom: {
    paddingTop: 25,
    paddingBottom: 20
  },
  roundViewActive: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundTextActive: {
    color: themePrimaryColor,
    fontSize: 10,
    fontWeight: 'bold'
  },
  roundViewBlock: {
    backgroundColor: themeQuaternaryColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1
  },
  roundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundText: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  moveCenter: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalButton: {
    backgroundColor: themeQuaternaryColor
  },
  modalHeight: {
    height: deviceHeight / 2.5
  },
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: deviceHeight / 2
  },
  smPadBottom: {
    paddingBottom: 5
  }
};

export default AppointmentProfessionalCare2;
