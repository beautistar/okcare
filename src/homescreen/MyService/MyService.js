import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Label,
  Button,
  Icon,
  Input,
  Content,
  Text,
  List,
  ListItem,
  Card,
  CardItem,
  Thumbnail
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const theme = require('../../theme/index.js');

class MyService extends Component {
  render() {
    return (
      <View>
        <Content>
          <View>
            <CardItem header>
              <Left/>
              <Body>
                <Text style={_styles.myServiceText}>{localized.myservice.professional}</Text>
              </Body>
              <Right>
                <Thumbnail style={_styles.myServiceRoundView}>
                  <Text style={_styles.myServiceRoundText}>10{localized.myservice.sheet}</Text>
                </Thumbnail>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.myservice.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.myservice.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.expiry}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>
                      {localized.myservice.date}</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem footer style={_styles.myServiceFooter}>
              <Button block style={_styles.myServiceButton1} onPress={() => Actions.AppointmentProfessionalCare1()}>
                <Text>{localized.myservice.appointment}</Text>
              </Button>
              <Button block style={_styles.myServiceButton2}>
                <Text>{localized.myservice.buy}</Text>
              </Button>
            </CardItem>
          </View>
          <View>
            <CardItem header>
              <Left/>
              <Body>
                <Text style={_styles.myServiceText}>{localized.myservice.professional}</Text>
              </Body>
              <Right>
                <Thumbnail style={_styles.myServiceRoundView}>
                  <Text style={_styles.myServiceRoundText}>10{localized.myservice.sheet}</Text>
                </Thumbnail>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.myservice.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.myservice.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.expiry}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>
                      {localized.myservice.date}</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem footer style={_styles.myServiceFooter}>
              <Button block style={_styles.myServiceButton1}>
                <Text>{localized.myservice.appointment}</Text>
              </Button>
              <Button block style={_styles.myServiceButton2}>
                <Text>{localized.myservice.buy}</Text>
              </Button>
            </CardItem>
          </View>
          <View>
            <CardItem header>
              <Left/>
              <Body>
                <Text style={_styles.myServiceText}>{localized.myservice.professional}</Text>
              </Body>
              <Right>
                <Thumbnail style={_styles.myServiceRoundView}>
                  <Text style={_styles.myServiceRoundText}>10{localized.myservice.sheet}</Text>
                </Thumbnail>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.myservice.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.myservice.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.myservice.expiry}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>
                      {localized.myservice.date}</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem footer style={_styles.myServiceFooter}>
              <Button block style={_styles.myServiceButton1}>
                <Text>{localized.myservice.appointment}</Text>
              </Button>
              <Button block style={_styles.myServiceButton2}>
                <Text>{localized.myservice.buy}</Text>
              </Button>
            </CardItem>
          </View>
        </Content>
      </View>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  textBold: {
    fontWeight: 'bold'
  },
  myServiceRoundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  myServiceRoundText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold'
  },
  myServiceText: {
    color: themePrimaryColor,
    fontWeight: 'bold',
    fontSize: 20
  },
  myServiceFooter: {
    justifyContent: 'space-between'
  },
  myServiceButton1: {
    backgroundColor: themePrimaryColor,
    width: (deviceWidth / 2) - 25
  },
  myServiceButton2: {
    backgroundColor: themeQuaternaryColor,
    width: (deviceWidth / 2) - 25
  },
  moveCenter: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  smPadTopBottom: {
    paddingTop: 5,
    paddingBottom: 5
  }
};

export default MyService;
