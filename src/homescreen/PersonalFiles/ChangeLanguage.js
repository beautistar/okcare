import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  ListItem,
  CheckBox,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
const theme = require('../../theme/index.js');
import localized from '../../Localized';

class ChangeLanguage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLang: "English"
    }
  }

  _goBack() {
    Actions.pop();
  }

  _save() {
    if (this.state.selectedLang === "English")
      localized.setLanguage('en-US');
    if (this.state.selectedLang === "Chinese")
      localized.setLanguage('zh');

    Actions.Signin();
  }

  _changeLanguage() {
    if (this.state.selectedLang === "English")
      this.setState({selectedLang: "Chinese"});
    if (this.state.selectedLang === "Chinese")
      this.setState({selectedLang: "English"});
    }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.language.title}</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this._save()}>
              <Icon name="md-checkmark" style={_styles.whiteColor}></Icon>
            </Button>
          </Right>
        </Header>
        <Content padder>
          <View>
            <ListItem>
              <CheckBox checked={this.state.selectedLang === "English"} onPress={() => this._changeLanguage()}/>
              <Body>
                <Text>{localized.language.english}</Text>
              </Body>
            </ListItem>
            <ListItem>
              <CheckBox checked={this.state.selectedLang === "Chinese"} onPress={() => this._changeLanguage()}/>
              <Body>
                <Text>{localized.language.chinese}</Text>
              </Body>
            </ListItem>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 3
  }
};

export default ChangeLanguage;
