import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Item,
  Text,
  Form,
  Thumbnail,
  Input,
  Icon,
  Label,
  ListItem,
  Left,
  Body,
  Right,
  CheckBox
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import ActionSheet from 'react-native-actionsheet'
import Hr from '../../lib/hr';
const theme = require('../../theme/index.js');
const cameraImg = require('../../assets/camera.png');
import localized from '../../Localized';

const CANCEL_INDEX = 0;
const options = [localized.personalfile.cancel, localized.personalfile.album, localized.personalfile.camera]
const title = localized.personalfile.actionsheetTitle

class PersonalFiles extends Component {
  constructor(props) {
    super(props);
    this._handleActionSheetClick = this._handleActionSheetClick.bind(this);
  }

  _goBack() {
    Actions.pop();
  }
  _logOut() {
    Actions.Signin();
  }

  _openActionSheet() {
    this.ActionSheet.show()
  }

  _handleActionSheetClick(i) {
    console.log(i);
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body>
            <Title style={_styles.whiteColor}>{localized.personalfile.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <View style={_styles.profileBGcolor}>
            <View style={_styles.name}>
              <Left/>
              <Body>
                <View style={_styles.thumbBGColor}></View>
              </Body>
              <Right>
                <Button transparent onPress={() => this._openActionSheet()}>
                  <Image style={_styles.cameraIcon} source={cameraImg}/>
                </Button>
              </Right>
            </View>
            <View style={_styles.name}>
              <Left/>
              <Body>
                <Text>Choi Ka Ka</Text>
              </Body>
              <Right/>
            </View>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.Userinfo()}>
              <Text>{localized.personalfile.userinfo}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.ServiceUser()}>
              <Text>{localized.personalfile.servicedata}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem>
              <Text>{localized.personalfile.packagerecord}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.UserDiscount()}>
              <Text>{localized.personalfile.userdiscount}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem>
              <Text>{localized.personalfile.news}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem>
              <Text>{localized.personalfile.news}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.ContactUs()}>
              <Text>{localized.personalfile.contactus}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.AboutUs()}>
              <Text>{localized.personalfile.aboutus}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.TermsOfUsage()}>
              <Text>{localized.personalfile.terms}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingSmall}>
            <ListItem onPress={() => Actions.ChangeLanguage()} style={_styles.spaceBetween}>
              <Text>{localized.personalfile.language}</Text>
              <Text note>{localized.personalfile.languageSelected}</Text>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <Button block style={_styles.buttonPrimaryColor} onPress={() => this._logOut()}>
              <Text>{localized.personalfile.logout}</Text>
            </Button>
          </View>
        </Content>
        <ActionSheet ref={o => this.ActionSheet = o} title={title} options={options} cancelButtonIndex={CANCEL_INDEX} onPress={this._handleActionSheetClick}/>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  paddingSmall: {
    paddingLeft: 5,
    paddingRight: 5
  },
  paddingMid: {
    paddingLeft: 10,
    paddingRight: 10
  },
  name: {
    flexDirection: 'row',
    padding: 10
  },
  profileBGcolor: {
    backgroundColor: '#e4e4e4'
  },
  cameraIcon: {
    height: 30,
    width: 40
  },
  thumbBGColor: {
    backgroundColor: 'white',
    height: 80,
    width: 80,
    borderRadius: 50,
  },
  spaceBetween: {
    justifyContent: 'space-between'
  }
};

export default PersonalFiles;
