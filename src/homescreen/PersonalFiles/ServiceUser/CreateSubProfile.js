import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Text,
  Icon,
  Input,
  Left,
  Body,
  Right,
  Item,
  CheckBox
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from "react-native-easy-grid";
import Hr from '../../../lib/hr';
const theme = require('../../../theme/index.js');
import localized from '../../../Localized';

class CreateSubProfile extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body>
            <Title style={_styles.whiteColor}>{localized.createsubprofile.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content style={_styles.midPadding}>
          <View>
            <Grid>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.absservation}:</Text>
                </Col>
                <Col>
                  <Input placeholder={localized.createsubprofile.absservationText} placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Col size={2}>
                  <Row>
                    <Text style={_styles.name}>{localized.createsubprofile.fname}</Text>
                    <Input placeholder="Choi" placeholderTextColor={themePrimaryColor}/>
                  </Row>
                </Col>
                <Col size={2}>
                  <Row>
                    <Text style={_styles.name}>{localized.createsubprofile.lname}</Text>
                    <Input placeholder="Ka Ka" placeholderTextColor={themePrimaryColor}/>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col size={1}>
                  <Row>
                    <Text style={_styles.gender}>{localized.createsubprofile.male}</Text><CheckBox checked={true}/></Row>
                </Col>
                <Col size={1}>
                  <Row>
                    <Text style={_styles.gender}>{localized.createsubprofile.female}</Text><CheckBox checked={false}/></Row>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.hRule}>
            <Hr/>
          </View>
          <View>
            <Grid>
              <Row>
                <Col>
                  <Text>{localized.createsubprofile.dob}:</Text>
                </Col>
                <Col>
                  <Text style={_styles.dob}>{localized.createsubprofile.dobText}</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.hRule}>
            <Hr/>
          </View>
          <View>
            <Grid>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.idnum}:</Text>
                </Col>
                <Col>
                  <Input placeholder="v056666" placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.h}:</Text>
                </Col>
                <Col>
                  <Input placeholder="160cm-165cm" placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.w}:</Text>
                </Col>
                <Col>
                  <Input placeholder="60kg-65kg" placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.relation}:</Text>
                </Col>
                <Col>
                  <Input placeholder={localized.createsubprofile.relationText} placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Text>{localized.createsubprofile.medical}:</Text>
              </Row>
              <Row>
                <Input placeholder={localized.createsubprofile.medicalText} placeholderTextColor={themePrimaryColor}></Input>
              </Row>
              <Row>
                <Text>{localized.createsubprofile.healthService}</Text>
              </Row>
              <Row style={_styles.paddTopBottom}>
                <Col size={1}>
                  <Row>
                    <Text style={_styles.healthService}>{localized.createsubprofile.no}</Text><CheckBox checked={true}/></Row>
                </Col>
                <Col size={1}>
                  <Row>
                    <Text style={_styles.healthService}>OK Care</Text><CheckBox checked={false}/></Row>
                </Col>
                <Col size={1}>
                  <Row>
                    <Text style={_styles.healthService}>{localized.createsubprofile.other}</Text><CheckBox checked={false}/></Row>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTop}>
            <Grid>
              <Row>
                <Col style={_styles.moveCenter}>
                  <Text style={_styles.textBold}>{localized.createsubprofile.address}</Text>
                </Col>
              </Row>
              <Row>
                <Col size={2}>
                  <Row>
                    <Text style={_styles.address}>{localized.createsubprofile.room}</Text>
                    <Input placeholder="A" placeholderTextColor={themePrimaryColor}/>
                  </Row>
                </Col>
                <Col size={2}>
                  <Row>
                    <Text style={_styles.address}>{localized.createsubprofile.floor}</Text>
                    <Input placeholder="50" placeholderTextColor={themePrimaryColor}/>
                  </Row>
                </Col>
                <Col size={2}>
                  <Row>
                    <Text style={_styles.address}>{localized.createsubprofile.seat}</Text>
                    <Input placeholder="1" placeholderTextColor={themePrimaryColor}/>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Input placeholder={localized.createsubprofile.longdoor} placeholderTextColor={themePrimaryColor}/>
              </Row>
              <Row>
                <Col size={2}>
                  <Input placeholder={localized.createsubprofile.addSelect1} placeholderTextColor={themePrimaryColor}/>
                </Col>
                <Col size={2}>
                  <Input placeholder={localized.createsubprofile.addSelect2} placeholderTextColor={themePrimaryColor}/>
                </Col>
              </Row>
              <Row>
                <Text>{localized.createsubprofile.number}</Text>
              </Row>
              <Row>
                <Input placeholder="60606066" placeholderTextColor={themePrimaryColor}/>
              </Row>
              <Row>
                <Text>{localized.createsubprofile.homenumber}</Text>
              </Row>
              <Row>
                <Input placeholder="2222222" placeholderTextColor={themePrimaryColor}/>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTop}>
            <Grid>
              <Row>
                <Col style={_styles.moveCenter}>
                  <Text style={_styles.textBold}>{localized.createsubprofile.emergency}</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.emergencycall}:</Text>
                </Col>
                <Col>
                  <Input placeholder={localized.createsubprofile.emergencycallText} placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={_styles.padTop}>{localized.createsubprofile.emergencynumber}:</Text>
                </Col>
                <Col>
                  <Input placeholder="99906066" placeholderTextColor={themePrimaryColor}></Input>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.paddTopBottom}>
            <Button block style={_styles.buttonQuaternaryColor}>
              <Text>{localized.createsubprofile.store}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  buttonQuaternaryColor: {
    backgroundColor: themeQuaternaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBold: {
    fontWeight: 'bold'
  },
  padTop: {
    paddingTop: 13
  },
  midPadding: {
    paddingRight: 20,
    paddingLeft: 20
  },
  name: {
    paddingTop: 13
  },
  gender: {
    paddingRight: 5
  },
  healthService: {
    paddingRight: 5
  },
  dob: {
    color: themePrimaryColor
  },
  hRule: {
    paddingTop: 10,
    paddingBottom: 10
  },
  paddTopBottom: {
    paddingTop: 10,
    paddingBottom: 10
  },
  address: {
    paddingTop: 13
  }
};

export default CreateSubProfile;
