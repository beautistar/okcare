import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Thumbnail,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../../Localized';
import ActionButton from '../../../lib/ActionButton/ActionButton';
import Hr from '../../../lib/hr';
const theme = require('../../../theme/index.js');

class PackagePurchaseRecord extends Component {
  _goBack() {
    Actions.pop();
  }

  _actionClick() {
    console.log("m41");
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.packagepurchaserecord.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <View>
            <View style={_styles.moveCenter}>
              <Text style={_styles.date}>{localized.packagepurchaserecord.date}</Text>
              <Hr style={_styles.quaternaryColor}/>
            </View>
            <List>
              <ListItem>
                <Body>
                  <Text>{localized.packagepurchaserecord.header}</Text>
                  <View style={_styles.rowView}>
                    <Text>{localized.packagepurchaserecord.state}:</Text>
                    <Text style={_styles.textBold}>{localized.packagepurchaserecord.stateData}</Text>
                  </View>
                  <View style={_styles.rowView}>
                    <Text>{localized.packagepurchaserecord.total}:</Text>
                    <Text style={_styles.textBold}>{localized.packagepurchaserecord.totalData}</Text>
                  </View>
                  <Text>{localized.packagepurchaserecord.purchase}</Text>
                </Body>
                <Right>
                  <Thumbnail style={_styles.roundView}>
                    <Text style={_styles.roundText}>{localized.packagepurchaserecord.reciept}</Text>
                  </Thumbnail>
                </Right>
              </ListItem>
              <Hr style={_styles.primaryColor}/>
              <ListItem>
                <Body>
                  <Text>{localized.packagepurchaserecord.header}</Text>
                  <View style={_styles.rowView}>
                    <Text>{localized.packagepurchaserecord.state}:</Text>
                    <Text style={_styles.textBold}>{localized.packagepurchaserecord.stateData}</Text>
                  </View>
                  <View style={_styles.rowView}>
                    <Text>{localized.packagepurchaserecord.total}:</Text>
                    <Text style={_styles.textBold}>{localized.packagepurchaserecord.totalData}</Text>
                  </View>
                  <Text>{localized.packagepurchaserecord.purchase}</Text>
                </Body>
                <Right>
                  <Thumbnail style={_styles.roundView}>
                    <Text style={_styles.roundText}>{localized.packagepurchaserecord.reciept}</Text>
                  </Thumbnail>
                </Right>
              </ListItem>
              <Hr style={_styles.quaternaryColor}/>
            </List>
          </View>
        </Content>
        <ActionButton buttonColor={themePrimaryColor} onPress={() => this._actionClick()}/>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  quaternaryColor: {
    color: themeQuaternaryColor
  },
  rowView: {
    flexDirection: 'row'
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 3
  },
  textBold: {
    fontWeight: 'bold'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  date: {
    color: themePrimaryColor,
    paddingBottom: 10,
    paddingTop: 10
  },
  roundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  roundText: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold'
  }
};

export default PackagePurchaseRecord;
