import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import ActionButton from '../../../lib/ActionButton/ActionButton';
const theme = require('../../../theme/index.js');
import localized from '../../../Localized';

class ServiceUser extends Component {
  _goBack() {
    Actions.pop();
  }

  _actionClick() {
    console.log("m41");
    Actions.CreateSubProfile();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.serviceuser.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <View>
            <List>
              <ListItem onPress={() => Actions.PackagePurchaseRecord()}>
                <Body>
                  <Text>{localized.serviceuser.header1}</Text>
                  <Text note>{localized.serviceuser.data1}</Text>
                </Body>
                <Right>
                  <Icon name='arrow-forward' style={_styles.primaryColor}/>
                </Right>
              </ListItem>
              <ListItem onPress={() => Actions.PackagePurchaseRecord()}>
                <Body>
                  <Text>{localized.serviceuser.header2}</Text>
                  <Text note>{localized.serviceuser.data2}</Text>
                </Body>
                <Right>
                  <Icon name='arrow-forward' style={_styles.primaryColor}/>
                </Right>
              </ListItem>
            </List>
          </View>

        </Content>
        <ActionButton buttonColor={themePrimaryColor} onPress={() => this._actionClick()}/>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 3
  }
};

export default ServiceUser;
