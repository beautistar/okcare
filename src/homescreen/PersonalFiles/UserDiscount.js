import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Item,
  Input,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

import ActionButton from '../../lib/ActionButton/ActionButton';
import Hr from '../../lib/hr';
import Modal from '../../lib/Modal';
const theme = require('../../theme/index.js');
import localized from '../../Localized';

class UserDiscount extends Component {
  constructor(props) {
    super(props);
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

  _goBack() {
    Actions.pop();
  }

  _actionClick() {
    this.refs.ocmodal.open();
  }

  _renderModal() {
    return (
      <Modal ref={'ocmodal'} backdropPressToClose={false} swipeToClose={false} onClosed={this.onClose} onOpened={this.onOpen} onClosingState={this.onClosingState} position={'bottom'} style={_styles.modalHeight}>
        <Container>
          <Header>
            <Left/>
            <Body/>
            <Right>
              <Button transparent onPress={() => this.refs.ocmodal.close()}>
                <Icon name='md-close-circle' style={_styles.secondaryColor}/>
              </Button>
            </Right>
          </Header>
          <Content padder>
            <View>
              <Item rounded>
                <Input placeholder={localized.inputModal} placeholderTextColor={themePrimaryColor}/>
              </Item>
            </View>
            <Hr style={_styles.primaryColor}/>
            <View style={_styles.modalButtonView}>
              <Button block style={_styles.modalButton}>
                <Text>{localized.userdiscount.buttonModal}</Text>
              </Button>
            </View>
          </Content>
        </Container>
      </Modal>
    );
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.userdiscount.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <View>
            <List>
              <ListItem>
                <Body>
                  <View style={_styles.header}>
                    <Text style={_styles.textBold}>{localized.userdiscount.header}(Newuser01)</Text>
                  </View>
                  <View style={_styles.coupon}>
                    <Text>{localized.userdiscount.note}:</Text>
                    <Text>{localized.userdiscount.coupon1}</Text>
                  </View>
                  <View>
                    <Text style={_styles.remark}>{localized.userdiscount.remark}</Text>
                  </View>
                </Body>
              </ListItem>
              <Hr style={_styles.primaryColor}/>
              <ListItem>
                <Body>
                  <View style={_styles.header}>
                    <Text style={_styles.textBold}>{localized.userdiscount.header}(Rebuy02)</Text>
                  </View>
                  <View style={_styles.coupon}>
                    <Text>{localized.userdiscount.note}:</Text>
                    <Text>{localized.userdiscount.coupon1}</Text>
                  </View>
                  <View>
                    <Text style={_styles.remark}>{localized.userdiscount.remark}</Text>
                  </View>
                </Body>
              </ListItem>
              <Hr style={_styles.primaryColor}/>
            </List>
          </View>
        </Content>
        <ActionButton buttonColor={themePrimaryColor} onPress={() => this._actionClick()}/>
      {this._renderModal()}
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  secondaryColor: {
    color: themeSecondaryColor
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 3
  },
  textBold: {
    fontWeight: 'bold'
  },
  remark: {
    color: themePrimaryColor,
    fontSize: 13
  },
  coupon: {
    flexDirection: 'row',
    paddingBottom: 10
  },
  header: {
    paddingBottom: 10
  },
  modalHeight: {
    height: deviceHeight / 3
  },
  modalButtonView: {
    padding: 10
  },
  modalButton: {
    backgroundColor: themeQuaternaryColor
  }
};

export default UserDiscount;
