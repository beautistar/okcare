import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Item,
  Text,
  Form,
  Thumbnail,
  Input,
  Icon,
  Label,
  ListItem,
  Left,
  Body,
  Right,
  CheckBox
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from "react-native-easy-grid";
import Hr from '../../lib/hr';
const theme = require('../../theme/index.js');
const cameraImg = require('../../assets/camera.png');
import localized from '../../Localized';

class Userinfo extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.userinfo.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content padder>
          <View style={_styles.paddingMid}>
            <ListItem>
              <Grid>
                <Row>
                  <Col size={2}>
                    <Row>
                      <Text style={_styles.name}>{localized.userinfo.fname}</Text>
                      <Input placeholder="Choi"/>
                    </Row>
                  </Col>
                  <Col size={2}>
                    <Row>
                      <Text style={_styles.name}>{localized.userinfo.lname}</Text>
                      <Input placeholder="Ka Ka"/>
                    </Row>
                  </Col>
                </Row>
              </Grid>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <ListItem>
              <Grid>
                <Row>
                  <Col size={1}>
                    <Row>
                      <Text style={_styles.gender}>{localized.userinfo.male}</Text><CheckBox checked={true}/></Row>
                  </Col>
                  <Col size={1}>
                    <Row>
                      <Text style={_styles.gender}>{localized.userinfo.female}</Text><CheckBox checked={false}/></Row>
                  </Col>
                </Row>
              </Grid>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <ListItem>
              <Text style={_styles.name}>{localized.userinfo.number}</Text>
              <Input placeholder="90903232"/>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <ListItem>
              <Text style={_styles.email}>{localized.userinfo.mail}</Text>
              <Input placeholder="choikaka@gmail.com"/>
            </ListItem>
          </View>
          <View style={_styles.paddingMid}>
            <ListItem style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.userinfo.setnewpass}</Text>
            </ListItem>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.setPassView}>
            <View>
              <Text style={_styles.name}>{localized.userinfo.oldpass}</Text>
              <Input placeholder={localized.userinfo.placeholderoldpwd}/>
            </View>
          </View>
          <View style={_styles.paddingMid}>
            <View>
              <Text style={_styles.name}>{localized.userinfo.newpass}</Text>
              <Input placeholder={localized.userinfo.placeholdernewpwd}/>
            </View>
          </View>
          <View style={_styles.paddingMid}>
            <Button block style={_styles.buttonPrimaryColor}>
              <Text>{localized.userinfo.save}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  primaryColor: {
    color: themePrimaryColor
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  heading: {
    flex: 3
  },
  whiteColor: {
    color: 'white'
  },
  paddingSmall: {
    paddingLeft: 5,
    paddingRight: 5
  },
  paddingMid: {
    paddingLeft: 10,
    paddingRight: 10
  },
  name: {
    paddingBottom: 5,
    fontWeight: 'bold'
  },
  gender: {
    paddingRight: 5,
    fontWeight: 'bold'
  },
  email: {
    paddingBottom: 2,
    fontWeight: 'bold'
  },
  setPassView: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5
  }
};

export default Userinfo;
