import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Thumbnail,
  CheckBox,
  Input
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";
const theme = require('../../theme/index.js');
import Hr from '../../lib/hr';

class PurchaseProfessionalCare1 extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.purchaseprofessionalcare1.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Header style={_styles.secondHeader} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          {/* <Left/> */}
          <Body style={_styles.heading2}>
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>1</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare1.config}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare1.service}</Text>
          </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
          <View style={_styles.roundView}>
              <Text style={_styles.roundText}>2</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare1.fill}</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare1.data}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundView}>
              <Text style={_styles.roundText}>3</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare1.carryout}</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare1.buy}</Text>
            </View>
          </Body>
          {/* <Right/> */}
        </Header>
        <Content padder>
          <View style={_styles.moveCenter}>
            <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare1.servicearea}</Text>
          </View>
          <View style={_styles.service}>
            <Text>{localized.purchaseprofessionalcare1.service}A,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}A,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}A,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}A,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}A</Text>
          </View>
          <View style={_styles.service}>
            <Text>{localized.purchaseprofessionalcare1.service}B,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}B,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}B,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}B,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}B</Text>
          </View>
          <View style={_styles.service}>
            <Text>{localized.purchaseprofessionalcare1.service}C,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}C,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}C,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}C,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}C</Text>
          </View>
          <View style={_styles.service}>
            <Text>{localized.purchaseprofessionalcare1.service}D,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}D,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}D,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}D,</Text>
            <Text>{localized.purchaseprofessionalcare1.service}D</Text>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row style={_styles.padTopBottom}>
                <Col size={4} style={_styles.moveCenter}>
                  <Text>{localized.purchaseprofessionalcare1.numberOfHours}</Text>
                </Col>
                <Col size={4} style={_styles.moveCenter}>
                  <Text>{localized.purchaseprofessionalcare1.numberOfDays}</Text>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare1.hours1}</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={true}/>
                </Col>
                <Col size={4}>
                  <Row>
                    <Col style={{
                      marginTop: -13
                    }} size={2}>
                      <Input placeholder="10"/>
                    </Col>
                    <Col size={4}>
                      <Text>{localized.purchaseprofessionalcare1.days}
                        X $400</Text>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare1.hours2}</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={false}/>
                </Col>
                <Col size={4}>
                  <Hr style={_styles.primaryColor}></Hr>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare1.hours3}</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={false}/>
                </Col>
                <Col size={4} style={_styles.moveCenter}>
                  <Text>{localized.purchaseprofessionalcare1.total}
                    : $4000</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.moveCenter}>
            <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare1.plusService}</Text>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row style={_styles.padTopBottom}>
                <Col size={4}>
                  <Text>{localized.purchaseprofessionalcare1.plusServiceA}:</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={true}/>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={4}>
                  <Text>{localized.purchaseprofessionalcare1.plusServiceB}:</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={false}/>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={4}>
                  <Text>{localized.purchaseprofessionalcare1.plusServiceC}:</Text>
                </Col>
                <Col size={1}>
                  <CheckBox checked={false}/>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row style={_styles.padTopBottom}>
                <Col size={2}>
                  <Text>{localized.purchaseprofessionalcare1.numberOfDays}</Text>
                </Col>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare1.days1}</Text>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={2}>
                  <Text>{localized.purchaseprofessionalcare1.plusService}</Text>
                </Col>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare1.days2}</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.moveCenter}>
            <Text style={_styles.textBold}>{localized.purchaseprofessionalcare1.total}
              : $7000</Text>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.moveCenter}>
            <Text>{localized.purchaseprofessionalcare1.expectedstart}
              :
              <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare1.date1}</Text>
            </Text>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.moveCenter}>
            <Text style={_styles.smallFont}>{localized.purchaseprofessionalcare1.terms}</Text>
          </View>
          <View style={_styles.padTopBottom}>
            <Button block style={_styles.buttonPrimaryColor} onPress={() => Actions.PurchaseProfessionalCare2()}>
              <Text>{localized.purchaseprofessionalcare1.next}</Text>
            </Button>
          </View>
          <View style={_styles.terms}>
            <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare1.terms2}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  secondHeader: {
    backgroundColor: themePrimaryColor,
    marginTop: -5,
    height: 90
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  textBold: {
    fontWeight: 'bold'
  },
  heading: {
    flex: 4,
    flexDirection: 'row'
  },
  heading2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 50,
    paddingRight: 50,
  },
  rowView: {
    flexDirection: 'row'
  },
  roundViewActive: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundTextActive: {
    color: themePrimaryColor,
    fontSize: 10,
    fontWeight: 'bold'
  },
  roundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundText: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  service: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5
  },
  padTopBottom: {
    paddingTop: 10,
    paddingBottom: 10
  },
  smallFont: {
    fontSize: 12
  },
  terms: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20
  }
};

export default PurchaseProfessionalCare1;
