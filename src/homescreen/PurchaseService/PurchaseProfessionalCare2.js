import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Thumbnail,
  Input,
  CheckBox
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";
const theme = require('../../theme/index.js');
import Hr from '../../lib/hr';

class PurchaseProfessionalCare2 extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.purchaseprofessionalcare2.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Header style={_styles.secondHeader} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          {/* <Left/> */}
          <Body style={_styles.heading2}>
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>1</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare2.config}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare2.service}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>2</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare2.fill}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare2.data}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundView}>
              <Text style={_styles.roundText}>3</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare2.carryout}</Text>
              <Text style={_styles.roundText}>{localized.purchaseprofessionalcare2.buy}</Text>
            </View>
          </Body>
          {/* <Right/> */}
        </Header>
        <Content padder>
          <View style={_styles.moveCenter}>
            <Text>{localized.purchaseprofessionalcare2.newserviceuser}</Text>
          </View>
          <View style={_styles.padLeftRight}>
            <View style={_styles.padTopBottom}>
              <Hr></Hr>
            </View>
            <View>
              <Grid>
                <Row>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.fname}/>
                  </Col>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.lname}/>
                  </Col>
                  <Col size={2} style={_styles.padTop}>
                    <Row>
                      <Text>{localized.purchaseprofessionalcare2.male}</Text>
                      <CheckBox checked={true}/>
                    </Row>
                  </Col>
                  <Col size={2} style={_styles.padTop}>
                    <Row>
                      <Text>{localized.purchaseprofessionalcare2.female}</Text>
                      <CheckBox checked={false}/>
                    </Row>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr></Hr>
          </View>
          <View style={_styles.padLeftRight}>
            <View>
              <Grid>
                <Row>
                  <Col>
                    <Text>{localized.purchaseprofessionalcare2.dob}:</Text>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare2.dobText}</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr></Hr>
          </View>
          <View style={_styles.padLeftRight}>
            <View>
              <Grid>
                <Row>
                  <Col size={2}>
                    <Text style={_styles.padTop}>{localized.purchaseprofessionalcare2.age}</Text>
                  </Col>
                  <Col size={3}>
                    <Input placeholder={localized.purchaseprofessionalcare2.idnum}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col size={1}>
                    <Input placeholder={localized.purchaseprofessionalcare2.h}></Input>
                  </Col>
                  <Col size={1}>
                    <Input placeholder={localized.purchaseprofessionalcare2.w}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Input placeholder={localized.purchaseprofessionalcare2.relation}></Input>
                  </Col>
                </Row>
                <Row>
                  <Input placeholder={localized.purchaseprofessionalcare2.medical}></Input>
                </Row>
                <Row style={_styles.moveCenter}>
                  <Text>{localized.purchaseprofessionalcare2.healthService}</Text>
                </Row>
                <Row style={_styles.padTopBottom}>
                  <Col size={1}>
                    <Row>
                      <Text style={_styles.healthService}>{localized.purchaseprofessionalcare2.no}</Text><CheckBox checked={true}/></Row>
                  </Col>
                  <Col size={1}>
                    <Row>
                      <Text style={_styles.healthService}>OK Care</Text><CheckBox checked={false}/></Row>
                  </Col>
                  <Col size={1}>
                    <Row>
                      <Text style={_styles.healthService}>{localized.purchaseprofessionalcare2.other}</Text><CheckBox checked={false}/></Row>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.moveCenter}>
            <Text>{localized.purchaseprofessionalcare2.address}</Text>
          </View>
          <View style={_styles.padLeftRight}>
            <View style={_styles.padTopBottom}>
              <Hr></Hr>
            </View>
            <View>
              <Grid>
                <Row>
                  <Col size={2} style={_styles.rowView}>
                    <Text style={_styles.padTop}>{localized.purchaseprofessionalcare2.room}</Text><Input/>
                  </Col>
                  <Col size={2} style={_styles.rowView}>
                    <Text style={_styles.padTop}>{localized.purchaseprofessionalcare2.floor}</Text><Input/>
                  </Col>
                  <Col size={2} style={_styles.rowView}>
                    <Text style={_styles.padTop}>{localized.purchaseprofessionalcare2.seat}</Text><Input/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Input placeholder={localized.purchaseprofessionalcare2.house}/>
                  </Col>
                </Row>
                <Row>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.area}/>
                  </Col>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.area}/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Input placeholder={localized.purchaseprofessionalcare2.number}/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Input placeholder={localized.purchaseprofessionalcare2.hnumber}/>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.moveCenter}>
            <Text>{localized.purchaseprofessionalcare2.emergency}</Text>
          </View>
          <View style={_styles.padLeftRight}>
            <View style={_styles.padTopBottom}>
              <Hr></Hr>
            </View>
            <View>
              <Grid>
                <Row>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.call}/>
                  </Col>
                  <Col size={2}>
                    <Input placeholder={localized.purchaseprofessionalcare2.contactTelephone}/>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Button block style={_styles.buttonPrimaryColor} onPress={() => Actions.PurchaseProfessionalCare3()}>
              <Text>{localized.purchaseprofessionalcare2.next}</Text>
            </Button>
          </View>
          <View style={_styles.terms}>
            <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare2.terms}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  secondHeader: {
    backgroundColor: themePrimaryColor,
    marginTop: -5,
    height: 90
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 4,
    flexDirection: 'row'
  },
  heading2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 50,
    paddingRight: 50,
  },
  rowView: {
    flexDirection: 'row'
  },
  roundViewActive: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundTextActive: {
    color: themePrimaryColor,
    fontSize: 10,
    fontWeight: 'bold'
  },
  roundView: {
    backgroundColor: themePrimaryColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundText: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  terms: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 20
  },
  padTopBottom: {
    paddingTop: 10,
    paddingBottom: 10
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  padTop: {
    paddingTop: 13
  }
};

export default PurchaseProfessionalCare2;
