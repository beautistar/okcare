import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Thumbnail,
  Input
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";
const theme = require('../../theme/index.js');
import Hr from '../../lib/hr';

class PurchaseProfessionalCare3 extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body style={_styles.heading}>
            <Title style={_styles.whiteColor}>{localized.purchaseprofessionalcare3.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Header style={_styles.secondHeader} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          {/* <Left/> */}
          <Body style={_styles.heading2}>
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>1</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.config}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.service}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>2</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.fill}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.data}</Text>
            </View>
            {/* <Text style={_styles.whiteColor}>——</Text> */}
            <View style={_styles.roundViewActive}>
              <Text style={_styles.roundTextActive}>3</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.carryout}</Text>
              <Text style={_styles.roundTextActive}>{localized.purchaseprofessionalcare3.buy}</Text>
            </View>
          </Body>
          {/* <Right/> */}
        </Header>
        <Content padder>
          <View style={_styles.padLeftRight}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare3.information}</Text>
            </View>
            <View style={_styles.rowViewWithPaddingTop}>
              <Text style={_styles.padRight}>{localized.purchaseprofessionalcare3.abbservation}</Text>
              <Text style={_styles.padLeft}>{localized.purchaseprofessionalcare3.name1}</Text>
            </View>
            <View style={_styles.rowViewWithPaddingTop}>
              <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.mail}:</Text>
              <Text style={_styles.padLeft}>healthlee@gmail.com</Text>
            </View>
            <View style={_styles.rowViewWithPaddingTop}>
              <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.number}:</Text>
              <Text style={_styles.padLeft}>6767 6767</Text>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare3.serviceuser}</Text>
            </View>
            <View style={_styles.padTop}>
              <Grid>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.padRight}>{localized.purchaseprofessionalcare3.abbservation}</Text>
                    <Text style={_styles.padLeft}>{localized.purchaseprofessionalcare3.name2}</Text>
                  </Col>
                  <Col style={_styles.moveCenter}>
                    <Text>ID：A0122122(3)</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col size={2} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.dob}
                      :
                    </Text>
                    <Text>{localized.purchaseprofessionalcare3.dob1}</Text>
                  </Col>
                  <Col size={1} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.age}
                      :
                    </Text>
                    <Text>{localized.purchaseprofessionalcare3.age1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col size={4} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.h}
                      :
                    </Text>
                    <Text>{localized.purchaseprofessionalcare3.h1}</Text>
                  </Col>
                  <Col size={3} style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.w}
                      :
                    </Text>
                    <Text>{localized.purchaseprofessionalcare3.w1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Text>{localized.purchaseprofessionalcare3.relation}</Text>
                </Row>
                <Row style={_styles.padTop}>
                  <Text>{localized.purchaseprofessionalcare3.medical}</Text>
                </Row>
                <Row style={_styles.padTop}>
                  <Col>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.address}
                      :
                    </Text>
                  </Col>
                  <Col>
                    <Text>{localized.purchaseprofessionalcare3.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.number}:</Text>
                    <Text>6767 6767</Text>
                  </Col>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.hnumber}:</Text>
                    <Text>2267 6767</Text>
                  </Col>
                </Row>
                <Row style={_styles.padTop}>
                  <Col style={_styles.rowView}>
                    <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.healthservice}:
                    </Text>
                    <Text>{localized.purchaseprofessionalcare3.no}</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare3.emergency}</Text>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row>
                <Col size={4}>
                  <Text>{localized.purchaseprofessionalcare3.amway}</Text>
                </Col>
                <Col size={2}>
                  <Text>6767 6767</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <View style={_styles.moveCenter}>
              <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare3.servicearea}</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.purchaseprofessionalcare3.service}A,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}A,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}A,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}A,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}A</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.purchaseprofessionalcare3.service}B,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}B,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}B,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}B,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}B</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.purchaseprofessionalcare3.service}C,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}C,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}C,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}C,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}C</Text>
            </View>
            <View style={_styles.service}>
              <Text>{localized.purchaseprofessionalcare3.service}D,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}D,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}D,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}D,</Text>
              <Text>{localized.purchaseprofessionalcare3.service}D</Text>
            </View>
          </View>
          <View>
            <View style={_styles.padLeftRight}>
              <Text>{localized.purchaseprofessionalcare3.plusServiceA}</Text>
            </View>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View>
            <Grid>
              <Row style={_styles.padTopBottom}>
                <Col size={2}>
                  <Text>{localized.purchaseprofessionalcare3.numberOfDays}</Text>
                </Col>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare3.days1}</Text>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={2}>
                  <Text>{localized.purchaseprofessionalcare3.plusService}</Text>
                </Col>
                <Col size={3}>
                  <Text>{localized.purchaseprofessionalcare3.days2}</Text>
                </Col>
              </Row>
              <Row style={_styles.padTopBottom}>
                <Col size={2}>
                  <Text>{localized.purchaseprofessionalcare3.voucher}</Text>
                </Col>
                <Col size={3}>
                  <Text>-$300</Text>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr style={_styles.primaryColor}></Hr>
          </View>
          <View style={_styles.padTopBottom}>
            <Grid>
              <Row>
                <Col size={3}>
                  <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.expectedstart}:
                  </Text>
                  <Text>{localized.purchaseprofessionalcare3.startdate}</Text>
                </Col>
                <Col size={2} style={_styles.rowView}>
                  <Text style={_styles.textBold}>{localized.purchaseprofessionalcare3.total}:
                  </Text>
                  <Text>$6700</Text>
                </Col>
              </Row>
              <Row style={_styles.moveCenter}>
                <Text>{localized.purchaseprofessionalcare3.voucher}</Text>
              </Row>
            </Grid>
          </View>
          <View style={_styles.padTopBottom}>
            <Hr></Hr>
          </View>
          <View style={_styles.padBottom}>
            <View style={_styles.padLeftRight}>
              <Input placeholder={localized.purchaseprofessionalcare3.offer}/>
            </View>
          </View>
          <View style={_styles.padBottom}>
            <Hr></Hr>
          </View>
          <View style={_styles.padBottom}>
            <View style={_styles.padLeftRight}>
              <Button block style={_styles.buttonPrimaryColor}>
                <Text>{localized.purchaseprofessionalcare3.paymentpurchase}</Text>
              </Button>
              <View style={_styles.moveCenterPadding}>
                <Text style={_styles.primaryColor}>{localized.purchaseprofessionalcare3.terms}</Text>
              </View>
            </View>
          </View>

        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  secondHeader: {
    backgroundColor: themePrimaryColor,
    marginTop: -5,
    height: 90
  },
  primaryColor: {
    color: themePrimaryColor
  },
  whiteColor: {
    color: 'white'
  },
  heading: {
    flex: 4,
    flexDirection: 'row'
  },
  heading2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 50,
    paddingRight: 50,
  },
  rowView: {
    flexDirection: 'row'
  },
  roundViewActive: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  roundTextActive: {
    color: themePrimaryColor,
    fontSize: 10,
    fontWeight: 'bold'
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBold: {
    fontWeight: 'bold'
  },
  padRight: {
    paddingRight: 5
  },
  padLeft: {
    paddingLeft: 5
  },
  padTop: {
    paddingTop: 10
  },
  padBottom: {
    paddingBottom: 10
  },
  padTopBottom: {
    paddingTop: 10,
    paddingBottom: 10
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  rowViewWithPaddingTop: {
    flexDirection: 'row',
    paddingTop: 10
  },
  service: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5
  },
  moveCenterPadding: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 20
  }
};

export default PurchaseProfessionalCare3;
