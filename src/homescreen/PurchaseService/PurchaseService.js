import React, {Component} from 'react';
import {Image, View, TouchableOpacity} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Input,
  Content,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from 'react-native-easy-grid';
import localized from '../../Localized';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const theme = require('../../theme/index.js');

const assessmentService = require('../../assets/assessmentService.png');
const departureService = require('../../assets/departureService.png');
const elderlyCare = require('../../assets/elderlyCare.png');
const maternalAndChildCare = require('../../assets/maternalAndChildCare.png');
const professionalCare = require('../../assets/professionalCare.png');
const rehabilitationTreatment = require('../../assets/rehabilitationTreatment.png');

class PurchaseService extends Component {
  render() {
    return (
      <View>
        <Grid style={_styles.purchaseGrid}>
          <Row style={_styles.purchaseRow}>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity onPress={() => Actions.PurchaseProfessionalCare1()}>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={professionalCare}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.professional}</Text>
                </View>
              </TouchableOpacity>
            </Col>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={rehabilitationTreatment}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.rehab}</Text>
                </View>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={_styles.purchaseRow}>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={elderlyCare}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.elder}</Text>
                </View>
              </TouchableOpacity>
            </Col>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={departureService}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.departure}</Text>
                </View>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={_styles.purchaseRow}>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={maternalAndChildCare}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.maternal}</Text>
                </View>
              </TouchableOpacity>
            </Col>
            <Col style={_styles.purchaseCol}>
              <TouchableOpacity>
                <View style={_styles.purchaseColView}>
                  <Image style={_styles.purchaseImage} source={assessmentService}/>
                  <Text style={_styles.whiteColor}>{localized.purchaseservice.assessment}</Text>
                </View>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  whiteColor: {
    color: 'white'
  },
  purchaseGrid: {
    padding: 5
  },
  purchaseRow: {
    height: (deviceWidth / 2) - 5,
    marginTop: 5
  },
  purchaseCol: {
    padding: 5
  },
  purchaseColView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: (deviceWidth / 2) - 10,
    backgroundColor: themePrimaryColor,
    borderRadius: 10
  },
  purchaseImage: {
    height: (deviceWidth / 4),
    width: (deviceWidth / 4),
    marginBottom: 5
  }
};

export default PurchaseService;
