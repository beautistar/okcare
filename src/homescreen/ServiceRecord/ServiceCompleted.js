import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Label,
  Button,
  Icon,
  Input,
  Content,
  Text,
  List,
  ListItem,
  Card,
  CardItem,
  Thumbnail
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";
import Modal from '../../lib/Modal';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const theme = require('../../theme/index.js');

import Hr from '../../lib/hr';

class ServiceCompleted extends Component {
  constructor(props) {
    super(props);
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

  _renderModal() {
    return (
      <Modal ref={'ocmodal'} backdropPressToClose={false} swipeToClose={false} onClosed={this.onClose} onOpened={this.onOpen} onClosingState={this.onClosingState} position={"bottom"} style={_styles.modalHeight}>
        <Container>
          <Header>
            <Left/>
            <Body style={_styles.heading}>
              <Title>{localized.servicecompleted.modalTitle}</Title>
            </Body>
            <Right>
              <Button transparent onPress={() => this.refs.ocmodal.close()}>
                <Icon name='md-close-circle' style={_styles.secondaryColor}/>
              </Button>
            </Right>
          </Header>
          <Content padder>
            <View style={_styles.modalHeart}>
              <Icon name="heart" onPress={() => this.refs.ocmodal.open()} style={_styles.secondaryColor}/>
              <Icon name="heart" onPress={() => this.refs.ocmodal.open()} style={_styles.secondaryColor}/>
              <Icon name="heart" onPress={() => this.refs.ocmodal.open()} style={_styles.secondaryColor}/>
              <Icon name="heart" onPress={() => this.refs.ocmodal.open()} style={_styles.secondaryColor}/>
              <Icon name="heart" onPress={() => this.refs.ocmodal.open()} style={_styles.secondaryColor}/>
            </View>
            <View style={_styles.smPaddingBottom}>
              <Item round>
                <Input multiline={true} placeholder={localized.servicecompleted.inputModal}/>
              </Item>
            </View>
            <Hr style={_styles.primaryColor}/>
            <View style={_styles.modalButtonView}>
              <Button block style={_styles.modalButton}>
                <Text>{localized.servicecompleted.buttonModal}</Text>
              </Button>
            </View>
          </Content>
        </Container>
      </Modal>
    );
  }

  render() {
    return (
      <View>
        <Content>
          <View style={_styles.serviceCompletedHeadView}>
            <Text style={_styles.serviceCompletedHeadText}>{localized.servicecompleted.date}</Text>
            <Text style={_styles.serviceCompletedHeadText}>{localized.servicecompleted.time}
            </Text>
          </View>
          <View>
            <CardItem header style={_styles.columnView}>
              <Text style={_styles.smPaddingBottom}>{localized.servicecompleted.professional}＋</Text>
              <Hr style={_styles.primaryColor}/>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.servicecompleted.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.servicecompleted.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.servicecompleted.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.servicecompleted.waiter}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>John Wong</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem footer>
              <Button block style={_styles.serviceCompletedQuaternaryButton}>
                <Text>{localized.servicecompleted.ratings}</Text>
              </Button>
            </CardItem>
          </View>
          <View style={_styles.serviceCompletedHeadView}>
            <Text style={_styles.serviceCompletedHeadText}>{localized.servicecompleted.date}</Text>
            <Text style={_styles.serviceCompletedHeadText}>{localized.servicecompleted.time}
            </Text>
          </View>
          <View>
            <CardItem header style={_styles.columnView}>
              <Text style={_styles.smPaddingBottom}>{localized.servicecompleted.professional}＋</Text>
              <Hr style={_styles.primaryColor}/>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.servicecompleted.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.servicecompleted.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.servicecompleted.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.servicecompleted.waiter}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>Chris Wong</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem style={_styles.serviceCompletedHR}>
              <Hr style={_styles.primaryColor}/>
            </CardItem>
            <CardItem>
              <Text style={_styles.textBold}>評分：
              </Text>
              <View style={_styles.rowView}>
                <Icon name="heart" onPress={() => this.refs.ocmodal.open()}/>
                <Icon name="heart" onPress={() => this.refs.ocmodal.open()}/>
                <Icon name="heart" onPress={() => this.refs.ocmodal.open()}/>
                <Icon name="heart" onPress={() => this.refs.ocmodal.open()}/>
              </View>
            </CardItem>
            <CardItem>
              <Item rounded style={_styles.serviceCompletedBigInput}>
                <Input multiline={true} placeholder={localized.servicecompleted.comment} placeholderTextColor={themePrimaryColor}/>
              </Item>
            </CardItem>
          </View>
        </Content>
        {this._renderModal()}
      </View>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  columnView: {
    flexDirection: 'column'
  },
  rowView: {
    flexDirection: 'row'
  },
  smPaddingBottom: {
    paddingBottom: 10
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  primaryColor: {
    color: themePrimaryColor
  },
  secondaryColor: {
    color: themeSecondaryColor
  },
  heading: {
    flex: 3
  },
  textBold: {
    fontWeight: 'bold'
  },
  serviceCompletedHeadView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10
  },
  serviceCompletedHeadText: {
    color: themePrimaryColor
  },
  serviceCompletedQuaternaryButton: {
    width: deviceWidth - (deviceWidth * 0.1),
    backgroundColor: themeQuaternaryColor
  },
  serviceCompletedHR: {
    paddingLeft: 15,
    paddingRight: 15
  },
  serviceCompletedBigInput: {
    paddingLeft: 15,
    paddingRight: 15,
    width: deviceWidth - (deviceWidth * 0.1),
    borderColor: themePrimaryColor
  },
  modalHeight: {
    height: deviceHeight / 2
  },
  modalHeart: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: deviceWidth - (deviceWidth * 0.8),
    paddingLeft: deviceWidth - (deviceWidth * 0.8)
  },
  modalButtonView: {
    padding: 10
  },
  modalButton: {
    backgroundColor: themeQuaternaryColor
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  smPadTopBottom: {
    paddingTop: 5,
    paddingBottom: 5
  }
};
export default ServiceCompleted;
