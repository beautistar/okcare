import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Label,
  Button,
  Icon,
  Input,
  Content,
  Text,
  List,
  ListItem,
  Card,
  CardItem,
  Thumbnail
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../../Localized';
import {Col, Row, Grid} from "react-native-easy-grid";

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const theme = require('../../theme/index.js');

import Hr from '../../lib/hr';

class ServiceProcessing extends Component {
  render() {
    return (
      <View>
        <Content>
          <View style={_styles.serviceProcessingHeadView}>
            <Text style={_styles.serviceProcessingHeadText}>{localized.serviceprocessing.date}</Text>
            <Text style={_styles.serviceProcessingHeadText}>{localized.serviceprocessing.time}
            </Text>
          </View>
          <View>
            <CardItem style={_styles.columnView}>
              <Text style={_styles.smPaddingBottom}>{localized.serviceprocessing.professional}＋</Text>
              <Hr style={_styles.primaryColor}/>
            </CardItem>
            <CardItem>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.serviceprocessing.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.serviceprocessing.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.serviceprocessing.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.serviceprocessing.waiter}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>John Wong</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem>
              <Button block style={_styles.serviceProcessingTertiaryButton}>
                <Icon name='md-call'/>
                <Text>6213 4531</Text>
              </Button>
            </CardItem>
          </View>
          <View style={_styles.serviceProcessingHeadView}>
            <Text style={_styles.serviceProcessingHeadText}>{localized.serviceprocessing.date}</Text>
            <Text style={_styles.serviceProcessingHeadText}>{localized.serviceprocessing.time}
            </Text>
          </View>
          <View>
            <CardItem header style={_styles.columnView}>
              <Text style={_styles.smPaddingBottom}>{localized.serviceprocessing.professional}＋</Text>
              <Hr style={_styles.primaryColor}/>
            </CardItem>
            <CardItem cardBody>
              <Grid style={_styles.padLeftRight}>
                <Row style={_styles.smPadTopBottom}>
                  <Label>{localized.serviceprocessing.name1}</Label>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.serviceprocessing.address}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>{localized.serviceprocessing.address1}</Text>
                  </Col>
                </Row>
                <Row style={_styles.smPadTopBottom}>
                  <Col size={2}>
                    <Text style={_styles.textBold}>{localized.serviceprocessing.waiter}：</Text>
                  </Col>
                  <Col size={4}>
                    <Text>Chris Wong</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem footer>
              <Button bordered style={_styles.serviceProcessingSecondaryButton}>
                <Text style={_styles.serviceProcessingSecondaryButtonText}>{localized.serviceprocessing.button2}</Text>
              </Button>
            </CardItem>
          </View>
        </Content>
      </View>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  columnView: {
    flexDirection: 'column'
  },
  smPaddingBottom: {
    paddingBottom: 10
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  primaryColor: {
    color: themePrimaryColor
  },
  textBold: {
    fontWeight: 'bold'
  },
  serviceProcessingHeadView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10
  },
  serviceProcessingHeadText: {
    color: themePrimaryColor
  },
  serviceProcessingTertiaryButton: {
    width: deviceWidth - (deviceWidth * 0.1),
    backgroundColor: themeTertiaryColor
  },
  serviceProcessingSecondaryButton: {
    width: deviceWidth - (deviceWidth * 0.1),
    borderColor: themeSecondaryColor,
    justifyContent: 'center'
  },
  serviceProcessingSecondaryButtonText: {
    color: themeSecondaryColor
  },
  padLeftRight: {
    paddingLeft: 10,
    paddingRight: 10
  },
  smPadTopBottom: {
    paddingTop: 5,
    paddingBottom: 5
  }
};
export default ServiceProcessing;
