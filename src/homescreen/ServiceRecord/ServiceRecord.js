import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Title,
  Header,
  Item,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Input,
  Content,
  Text
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {RadioButtons, SegmentedControls} from 'react-native-radio-buttons';
import localized from '../../Localized';
import ServiceCompleted from './ServiceCompleted';
import ServiceProcessing from './ServiceProcessing';

const theme = require('../../theme/index.js');

class ServiceRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomSegment: {
        label: localized.servicerecord.processing,
        value: 'processing'
      }
    };
  }

  renderView() {
    if (this.state.selectedCustomSegment.value === 'processing') {
      return (<ServiceProcessing/>);
    } else if (this.state.selectedCustomSegment.value === 'completed') {
      return (<ServiceCompleted/>);
    } else {
      return (
        <View>
          <Text>Please Select Any Option</Text>
        </View>
      );
    }
  }

  renderSegmentControl() {
    const options = [
      {
        label: localized.servicerecord.processing,
        value: 'processing'
      }, {
        label: localized.servicerecord.completed,
        value: 'completed'
      }
    ];

    function setSelectedOption(option) {
      this.setState({selectedCustomSegment: option});
    }

    return (
      <View style={{
        marginTop: 10
      }}>
        <SegmentedControls tint={themePrimaryColor} selectedTint={'white'} backTint={'white'} optionStyle={{
          margin: 5,
          fontWeight: 'bold'
        }} containerStyle={{
          marginLeft: 10,
          marginRight: 10
        }} options={options} onSelection={setSelectedOption.bind(this)} selectedOption={this.state.selectedCustomSegment} extractText={(option) => option.label} testOptionEqual={(a, b) => {
          if (!a || !b) {
            return false;
          }
          return a.label === b.label
        }}></SegmentedControls>{this.renderView()}
      </View>
    );
  }

  render() {
    return (

      <View>
        {this.renderSegmentControl()}
      </View>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {};

export default ServiceRecord;
