import React, {Component} from 'react';
import {Scene, Router} from 'react-native-router-flux';
import Home from './Home';
import LoginMain from '../loginscreen';
import PersonalFiles from './PersonalFiles/PersonalFiles';
import Userinfo from './PersonalFiles/Userinfo';
import ContactUs from './PersonalFiles/Contact/ContactUs';
import AboutUs from './PersonalFiles/Contact/AboutUs';
import TermsOfUsage from './PersonalFiles/Contact/TermsOfUsage';
import ServiceUser from './PersonalFiles/ServiceUser/ServiceUser';
import PackagePurchaseRecord from './PersonalFiles/ServiceUser/PackagePurchaseRecord';
import UserDiscount from './PersonalFiles/UserDiscount';
import ChangeLanguage from './PersonalFiles/ChangeLanguage';
import CreateSubProfile from './PersonalFiles/ServiceUser/CreateSubProfile';
import PurchaseProfessionalCare1 from './PurchaseService/PurchaseProfessionalCare1';
import PurchaseProfessionalCare2 from './PurchaseService/PurchaseProfessionalCare2';
import PurchaseProfessionalCare3 from './PurchaseService/PurchaseProfessionalCare3';
import AppointmentProfessionalCare1 from './MyService/AppointmentProfessionalCare1';
import AppointmentProfessionalCare2 from './MyService/AppointmentProfessionalCare2';

class HomeMain extends Component {
  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar hideTabBar>
          <Scene key="Home" component={Home} title="Home" initial='true' panHandlers={null}/>
          <Scene key="Signin" component={LoginMain} title="Signin" panHandlers={null}/>
          <Scene key="PersonalFiles" component={PersonalFiles} title="PersonalFiles" panHandlers={null}/>
          <Scene key="Userinfo" component={Userinfo} title="Userinfo" panHandlers={null}/>
          <Scene key="ContactUs" component={ContactUs} title="ContactUs" panHandlers={null}/>
          <Scene key="AboutUs" component={AboutUs} title="AboutUs" panHandlers={null}/>
          <Scene key="TermsOfUsage" component={TermsOfUsage} title="TermsOfUsage" panHandlers={null}/>
          <Scene key="ServiceUser" component={ServiceUser} title="ServiceUser" panHandlers={null}/>
          <Scene key="PackagePurchaseRecord" component={PackagePurchaseRecord} title="PackagePurchaseRecord" panHandlers={null}/>
          <Scene key="UserDiscount" component={UserDiscount} title="UserDiscount" panHandlers={null}/>
          <Scene key="ChangeLanguage" component={ChangeLanguage} title="ChangeLanguage" panHandlers={null}/>
          <Scene key="CreateSubProfile" component={CreateSubProfile} title="CreateSubProfile" panHandlers={null}/>
          <Scene key="PurchaseProfessionalCare1" component={PurchaseProfessionalCare1} title="PurchaseProfessionalCare1" panHandlers={null}/>
          <Scene key="PurchaseProfessionalCare2" component={PurchaseProfessionalCare2} title="PurchaseProfessionalCare2" panHandlers={null}/>
          <Scene key="PurchaseProfessionalCare3" component={PurchaseProfessionalCare3} title="PurchaseProfessionalCare3" panHandlers={null}/>
          <Scene key="AppointmentProfessionalCare1" component={AppointmentProfessionalCare1} title="AppointmentProfessionalCare1" panHandlers={null}/>
          <Scene key="AppointmentProfessionalCare2" component={AppointmentProfessionalCare2} title="AppointmentProfessionalCare2" panHandlers={null}/>
        </Scene>
      </Router>
    );
  }
}

export default HomeMain;
