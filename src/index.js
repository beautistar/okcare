import React, {Component} from 'react';
import Splash from './splashscreen';
import HomeMain from './homescreen';
import LoginMain from './loginscreen';
import {Platform, Dimensions} from 'react-native';

class app extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLogin: false
    };
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({isLoading: false})
    }, 4000);
  }

  render() {
    if (this.state.isLoading === true) {
      return (<Splash/>);
    } else {
      if (this.state.isLogin === true) {
        return (<HomeMain/>);
      } else {
        return (<LoginMain/>);
      }
    }
  }
}

export default app;
