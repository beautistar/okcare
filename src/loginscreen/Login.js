import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Item,
  Text,
  Form,
  Input,
  Icon,
  Label,
  Left,
  Body,
  Right
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import localized from '../Localized';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

import Hr from '../lib/hr';

const backgroundImg = require('../assets/mask.png');
const logoImg = require('../assets/logo.png');
const theme = require('../theme/index.js');

class Signin extends Component {
  _goSignup() {
    Actions.Register();
  }
  _doLogin() {
    Actions.Home();
  }
  render() {
    return (
      <Container>
        <Image style={_styles.backGroundImage} source={backgroundImg}>
          <Content style={{
            zIndex: 50,
            padding: 10
          }}>
            <View style={_styles.logoView}>
              <Image style={_styles.logoImage} source={logoImg}/>
              <Label style={_styles.logoText}>OK CARE</Label>
            </View>
            <View style={_styles.smPaddingBottom}>
              <Item>
                <Icon active name='contact' style={_styles.emailIcon}/>
                <Input placeholder={localized.login.uname}/>
              </Item>
            </View>
            <View style={_styles.smPaddingBottom}>
              <Item>
                <Icon active name='lock' style={_styles.passwordIcon}/>
                <Input secureTextEntry={true} placeholder={localized.login.password}/>
              </Item>
            </View>
            <View style={_styles.smPaddingBottom}>
              <Button block style={_styles.buttonPrimaryColor} onPress={() => this._doLogin()}>
                <Text>{localized.login.signin}</Text>
              </Button>
            </View>
            <View style={_styles.moveCenter}>
              <Text style={_styles.link}>{localized.login.forgetpassword}
              </Text>
            </View>
            <View style={_styles.hrPadding}>
              <Hr style={_styles.primaryColor}>
                <Text style={_styles.link}>
                  {localized.login.or}
                </Text>
              </Hr>
            </View>
            <View style={_styles.smPaddingBottom}>
              <Button block style={_styles.buttonSecondaryColor} onPress={() => this._goSignup()}>
                <Text>
                  {localized.login.signup}</Text>
              </Button>
            </View>
            <View style={_styles.moveCenter}>
              <Text>{localized.login.tcLine}</Text>
              <Text style={_styles.link}>{localized.login.termsOfUse}
              </Text>
            </View>
          </Content>
        </Image>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  smPaddingBottom: {
    paddingBottom: 10
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  primaryColor: {
    color: themePrimaryColor
  },
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  buttonSecondaryColor: {
    backgroundColor: themeSecondaryColor
  },
  backGroundImage: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    backgroundColor: 'transparent',
    zIndex: -50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 80
  },
  logoImage: {
    flex: 1,
    width: deviceWidth - (deviceWidth * 0.08),
    height: 100,
    resizeMode: 'contain',
    marginTop: 20
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 40,
    color: themePrimaryColor
  },
  hrPadding: {
    paddingBottom: 10,
    paddingTop: 10
  },
  link: {
    color: themeQuaternaryColor,
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  loginIcon: {
    width: 10
  },
  emailIcon: {
    fontSize: 40,
    color: themePrimaryColor
  },
  passwordIcon: {
    fontSize: 40,
    color: themeSecondaryColor
  }
};

export default Signin;
