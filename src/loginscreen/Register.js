import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {
  Container,
  Content,
  Title,
  Button,
  Header,
  Item,
  Text,
  Form,
  Input,
  Icon,
  Label,
  Left,
  Body,
  Right,
  CheckBox
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Col, Row, Grid} from "react-native-easy-grid";
import localized from '../Localized';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const theme = require('../theme/index.js');
const profileImg = require('../assets/profile.png');

class Signup extends Component {
  _goBack() {
    Actions.pop();
  }

  render() {
    return (
      <Container>
        <Header style={_styles.buttonPrimaryColor} iosBarStyle='light-content' androidStatusBarColor={themePrimaryColor}>
          <Left>
            <Button transparent onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={_styles.whiteColor}/>
            </Button>
          </Left>
          <Body>
            <Title style={_styles.whiteColor}>{localized.register.title}</Title>
          </Body>
          <Right/>
        </Header>
        <Content padder>
          <View style={_styles.logoView}>
            <Image style={_styles.profileImage} source={profileImg}/>
          </View>
          <View style={_styles.smPaddingBottom}>
            <Grid>
              <Row>
                <Col size={2}><Input placeholder={localized.register.fname}/></Col>
                <Col size={2}>
                  <Input placeholder={localized.register.lname}/></Col>
                <Col size={2} style={{
                  paddingTop: 15
                }}>
                  <Row>
                    <Text>{localized.register.male}</Text><CheckBox checked={true}/></Row>
                </Col>
                <Col size={2} style={{
                  paddingTop: 15
                }}>
                  <Row>
                    <Text>{localized.register.female}</Text><CheckBox checked={false}/></Row>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={_styles.smPaddingBottom}>
            <Item>
              <Input placeholder={localized.register.number}/>
            </Item>
          </View>
          <View style={_styles.smPaddingBottom}>
            <Item>
              <Input placeholder={localized.register.email}/>
            </Item>
          </View>
          <View style={_styles.smPaddingBottom}>
            <Item>
              <Input placeholder={localized.register.password}/>
            </Item>
          </View>
          <View style={_styles.smPaddingBottom}>
            <Button block style={_styles.buttonSecondaryColor}>
              <Text>{localized.register.signin}</Text>
            </Button>
          </View>
          <View style={_styles.moveCenter}>
            <Text style={_styles.quaternaryColor}>{localized.register.termsOfService}
            </Text>
          </View>
        </Content>
      </Container>
    );
  }
}

/***** Style Sheet *****/

const themePrimaryColor = theme.primaryColor;
const themeSecondaryColor = theme.secondaryColor;
const themeTertiaryColor = theme.tertiaryColor;
const themeQuaternaryColor = theme.quaternaryColor;

const _styles = {
  smPaddingBottom: {
    paddingBottom: 10
  },
  moveCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonPrimaryColor: {
    backgroundColor: themePrimaryColor
  },
  buttonSecondaryColor: {
    backgroundColor: themeSecondaryColor
  },
  whiteColor: {
    color: 'white'
  },
  quaternaryColor: {
    color: themeQuaternaryColor
  },
  logoView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 80
  },
  profileImage: {
    flex: 1,
    width: deviceWidth - (deviceWidth * 0.08),
    height: 150,
    resizeMode: 'contain',
    marginTop: 20
  }
};

export default Signup;
