import React, {Component} from 'react';
import {Scene, Router} from 'react-native-router-flux';
import Login from './Login';
import Register from './Register';
import HomeMain from '../homescreen';

class LoginMain extends Component {
  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar hideTabBar>
          <Scene key="Login" component={Login} title="Login" initial='true' panHandlers={null}/>
          <Scene key="Register" component={Register} title="Signup" panHandlers={null}/>
          <Scene key="Home" component={HomeMain} title="Home" panHandlers={null}/>
        </Scene>
      </Router>
    );
  }
}

export default LoginMain;
