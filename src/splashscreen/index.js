import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import {Label} from 'native-base';

import {Platform, Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const splashImg = require('../assets/splash_logo.png');

class Splash extends Component {
  render() {
    return (
      <View style={_styles.splashView}>
        <Image style={_styles.splashLogoImage} source={splashImg}/>
      </View>
    );
  }
}

const _styles = {
  splashView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  splashLogoImage: {
    width: deviceWidth,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain'
  }
};

export default Splash;
