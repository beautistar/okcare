module.exports = {
  primaryColor: '#00C7C8',
  secondaryColor: '#EF6D5A',
  tertiaryColor: '#65C39B',
  quaternaryColor: '#5CD0F2',
};
